/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game_v_03;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mike
 */
public class collu {
    private List<sprite> list = new ArrayList<sprite>();
    sprite e;
    
    collu(){}
    public void addObject(int x, int y, int height, int width, boolean isWall, boolean isFatal, boolean xon, boolean yon, int xmove, int ymove){
        e = new sprite(x, y, height, width, isWall, isFatal, xon, yon, xmove, ymove);
        list.add(e);
    }
    public void updateObject(int index, int x, int y, int height, int width, boolean isWall, boolean isFatal, boolean xon, boolean yon, int xmove, int ymove){
        list.get(index).updateSprite(x, y, height, width, isWall, isFatal, xon, yon, xmove, ymove);
    }
    public void moveObjects(){
        for (int i = 0; i < list.size(); i++){
            if ((list.get(i).getXon() == true) || (list.get(i).getYon() == true)){
                list.get(i).changeX();
                list.get(i).changeY();
            }
        }
    }
    public boolean isOnTop(int index, sprite sp){
        if((sp.getY() + sp.getHeight() + 1 == list.get(index).getY()) &&
        (sp.getX() + sp.getWidth() > list.get(index).getX()) &&
        (sp.getX() < list.get(index).getX() + list.get(index).getWidth())){
            return true;
        }
        return false;
    }
    public sprite getSprite(int index){
        return list.get(index);
    }
    public boolean checkBoxX(sprite sp, int xmove, int ymove){
        for (int i = 0; i < (list.size()); i++){
            /*if (i == 0){
                System.out.println("CHECK xmove: " + xmove + " ymove: " + ymove);
                System.out.println("obj y: " + list.get(i).getY() + "obj x: " + list.get(i).getX());
                System.out.println("obj width: " + list.get(i).getWidth() + "obj height: " + list.get(i).getHeight());
            }*/
            //if you are moving into a wall on the left from the right...
            if (sp.getX() > (list.get(i).getX() + list.get(i).getWidth())){
                //System.out.println("Passed at sp.getX() = " + sp.getX() + " > list.get(i).getX() + list.get(i).getWidth() = " + (list.get(i).getX() + list.get(i).getWidth()));
                
                //if your left most location plus the direction you are trying to go is less than or equal to
                //the walls left most location plus its width, you might be hitting
                if ((sp.getX() + (xmove)) <= (list.get(i).getX() + list.get(i).getWidth())){
                    //System.out.println("Passed at (sp.getX() + (xmove)) = " + (sp.getX() + (xmove)) + " <= (list.get(i).getX() + list.get(i).getWidth()) = " + (list.get(i).getX() + list.get(i).getWidth()));
                    //if your y location plus where you are going plus your height is >=
                    //the y location of the wall, you are NOT going over it (would have to be <=)
                    if ((sp.getY() + ymove-ymove + sp.getHeight()) >= (list.get(i).getY())){ //Account for gravity
                        //System.out.println("Passed at (sp.getY() + ymove + sp.getHeight()) = " + (sp.getY() + ymove + sp.getHeight()) + " > (list.get(i).getY()) = " + (list.get(i).getY()));
                        //if your y location plus where you are going is <= the y loc of the wall + its height
                        //you are NOT going under it (would have to be >=)
                        if ((sp.getY() + ymove) <= (list.get(i).getY() + list.get(i).getHeight())){
                            //System.out.println("COLLU at (sp.getY() + ymove)  = " + (sp.getY() + ymove) + " < (list.get(i).getY() + list.get(i).getHeight()) = " + (list.get(i).getY() + list.get(i).getHeight()));
                            //System.out.println("Setting X to: " + (list.get(i).getX() + list.get(i).getWidth() + 1));
                            //if a wall is to your left, and your xmovement will run you into it and you
                            //arent going over or under it, then you are hitting it SOUND TEH ALARMS
                            sp.setX(list.get(i).getX() + list.get(i).getWidth()+1);
                            if (list.get(i).getFatal()){
                                sp.setDead(true);
                            }
                            return true;}}}
            }
            if (sp.getX() + sp.getWidth() < list.get(i).getX()){
                if ((sp.getX() + sp.getWidth() + xmove) >= (list.get(i).getX())){
                    if ((sp.getY() + ymove-ymove + sp.getHeight()) >= (list.get(i).getY())){ //Account for gravity
                        if ((sp.getY() + ymove) <= (list.get(i).getY() + list.get(i).getHeight())){
                            sp.setX(list.get(i).getX() - sp.getWidth() - 1);
                            if (list.get(i).getFatal()){
                                sp.setDead(true);
                            }
                            return true;}}}
            }
        }
        return false;
    }
    public boolean checkBoxY(sprite sp, int xmove, int ymove){
        for (int i = 0; i < (list.size()); i++){
       /*     if (i == 0){
                System.out.println("CHECK xmove: " + xmove + " ymove: " + ymove);
                System.out.println("sprite y: " + list.get(i).getY() + "sprite x: " + list.get(i).getX());
                System.out.println("sprite width: " + list.get(i).getWidth() + "sprite height: " + list.get(i).getWidth());
            }*/
            if (sp.getY() > (list.get(i).getY() + list.get(i).getHeight())){
                //System.out.println("Passed at sp.getY() = " + sp.getY() + " > list.get(i).getY() + list.get(i).getHeight() = " + (list.get(i).getY() + list.get(i).getHeight()));
                if ((sp.getY() + (ymove)) <= (list.get(i).getY() + list.get(i).getHeight())){
                    //System.out.println("Passed at (sp.getY() + (ymove)) = " + (sp.getY() + (ymove)) + " <= (list.get(i).getY() + list.get(i).getHeight()) = " + (list.get(i).getY() + list.get(i).getHeight()));
                    if ((sp.getX() + xmove + sp.getWidth()) >= (list.get(i).getX())){
                        //System.out.println("Passed at (sp.getX() + xmove + sp.getWidth()) = " + (sp.getX() + xmove + sp.getWidth()) + " > (list.get(i).getX()) = " + (list.get(i).getX()));
                        if ((sp.getX() + xmove) <= (list.get(i).getX() + list.get(i).getWidth())){
                            //System.out.println("COLLU at (sp.getX() + xmove)  = " + (sp.getX() + xmove) + " < (list.get(i).getX() + list.get(i).getWidth()) = " + (list.get(i).getX() + list.get(i).getWidth()));
                            //System.out.println("Setting Y to: " + (list.get(i).getY() + list.get(i).getHeight() + 1));
                            sp.setY(list.get(i).getY() + list.get(i).getHeight() + 1);
                            if (list.get(i).getFatal()){
                                sp.setDead(true);
                            }
                            return true;}}}
            }
            if (sp.getY() + sp.getHeight() < list.get(i).getY()){
                if ((sp.getY() + sp.getHeight() + ymove) >= (list.get(i).getY())){
                    if ((sp.getX() + xmove + sp.getWidth()) >= (list.get(i).getX())){
                        if ((sp.getX() + xmove) <= (list.get(i).getX() + list.get(i).getWidth())){
                            sp.setY(list.get(i).getY() - sp.getHeight() - 1);
                            if (list.get(i).getFatal()){
                                sp.setDead(true);
                            }
                            return true;}}}
            }
        }
        return false;
    }
}
