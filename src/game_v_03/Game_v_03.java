/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game_v_03;

import java.applet.Applet;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class Game_v_03 extends Applet implements Runnable{
   //DEFAULT IS y = 0, x = 50

   final int WIDTH = 750;
   final int HEIGHT = 750;
   BufferedImage background = null, backgroundalt = null;
   BufferedImage playerleft = null, playerleftalt = null, playerright = null, playerrightalt = null, playerleftstride = null, playerleftstridealt = null, playerrightstride = null, playerrightstridealt = null;
   private int y = 50, ymove = 0, grassy = 150, testval = 0;
   private int x = 50, xmove = 1;
   private int idletime = 0, lefttime = 0, righttime = 0, prevDelta = 2, tempmove;
   private int[] test1 = {50,100,75};
   private int[] test2 = {(int)grassy,(int)grassy,(int)grassy-125};
   private int falltime = 0, jumpthres = 100;
   private boolean xon = false, yon = false, jump = false, leftoverride = false, rightoverride = false, canJump = true;
   Clip sounds;
   
   Calendar cal;
   SimpleDateFormat sdf;
   
   collu checker;
   sprite user;
   JFrame frame;
   Canvas canvas;
   BufferStrategy bufferStrategy;
   
    public Game_v_03(){
      frame = new JFrame("Basic Game");

              
    /*  try {
    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(getClass().getResource("/sounds/Arkham_Bridge.wav"));
    sounds = AudioSystem.getClip();
    sounds.open(audioInputStream);
    //sounds.start();
    } catch(Exception e) {
    e.printStackTrace();
    }
    sounds.loop(500); */ 
    
 
    

    
 /*   try {
            backgroundalt = ImageIO.read(getClass().getResource("/images/levels/level2Night.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }*/
      try {
            background = ImageIO.read(getClass().getResource("/images/levels/level3.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      checker = new collu();
      //test object
      //checker.addObject(0, 100, 30, 100, true, false, true, false, 1, 0);
      
      checker.addObject(0,150,390,30,true,false,false,false,0,0);
checker.addObject(30,150,30,300,true,false,false,false,0,0);
checker.addObject(300,90,60,30,true,false,false,false,0,0);
checker.addObject(300,180,30,270,true,false,false,false,0,0);
checker.addObject(330,90,20,60,true,false,false,false,0,0);
checker.addObject(480,90,20,60,true,false,false,false,0,0);
checker.addObject(540,90,90,30,true,false,false,false,0,0);
checker.addObject(630,90,120,120,true,false,false,false,0,0);
checker.addObject(630,300,120,120,true,false,false,false,0,0);
checker.addObject(540,330,30,60,true,false,false,false,0,0);
checker.addObject(540,300,30,30,true,false,false,false,0,0);
checker.addObject(207,390,30,450,true,false,false,false,0,0) ;    
checker.addObject(207,300,90,30,true,false,false,false,0,0);
checker.addObject(361,300,20,90,true,false,false,false,0,0);
checker.addObject(237,300,20,60,true,false,false,false,0,0);
checker.addObject(30,510,30,570,true,false,false,false,0,0);
checker.addObject(720,420,90,30,true,false,false,false,0,0);
checker.addObject(330,168,12,210,true,true,false,false,0,0);
checker.addObject(570,100,110,10,true,true,false,false,0,0);
checker.addObject(620,100,110,10,true,true,false,false,0,0);
checker.addObject(570,318,12,60,true,true,false,false,0,0);
checker.addObject(30,225,240,12,true,true,false,false,0,0);
checker.addObject(154,492,6,34,true,true,false,false,0,0);
checker.addObject(497,431,6,34,true,true,false,false,0,0);
checker.addObject(237,371,6,393,true,true,false,false,0,0);
checker.addObject(696,420,90,30,true,true,false,false,0,0);
checker.addObject(0,750,30,750,true,true,false,false,0,0);

//checker.addObject(testval, 450, 50, 200, true, false, true, true, 1, 0);
      user = new sprite(x, y, 30, 15, false, false, xon, yon, xmove, ymove);
      //PLAYER IMAGES      
      try {
            playerleft = ImageIO.read(getClass().getResource("/images/Character/CharPlayerback.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerright = ImageIO.read(getClass().getResource("/images/Character/CharPlayer.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerleftalt = ImageIO.read(getClass().getResource("/images/Character/CharPlayerbackAlt.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerrightalt = ImageIO.read(getClass().getResource("/images/Character/CharPlayerAlt.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerleftstride = ImageIO.read(getClass().getResource("/images/Character/CharBackStride1.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerrightstride = ImageIO.read(getClass().getResource("/images/Character/CharStride1.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerleftstridealt = ImageIO.read(getClass().getResource("/images/Character/CharBackStride2.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      try {
            playerrightstridealt = ImageIO.read(getClass().getResource("/images/Character/CharStride2.png"));
        } catch (IOException ex) {
            Logger.getLogger(Game_v_03.class.getName()).log(Level.SEVERE, null, ex);
        }
      
      JPanel panel = (JPanel) frame.getContentPane();
      panel.setPreferredSize(new Dimension(WIDTH, HEIGHT));
      panel.setLayout(null);
      
      canvas = new Canvas();
      canvas.setBounds(0, 0, WIDTH, HEIGHT);
      canvas.setIgnoreRepaint(true);
      
      panel.add(canvas);
      
      canvas.addMouseListener(new MouseControl());
      canvas.addKeyListener(new KeyListener() {

           @Override
           public void keyTyped(KeyEvent e) {
               
           }

           @Override
           public void keyPressed(KeyEvent e) {
               //System.out.println(e.getKeyCode());
               if ((e.getKeyCode() == 37)){
                   xmove = -1;
                   xon = true;
                   leftoverride = true;
               }
               else if (e.getKeyCode() == 38){
                   ymove = -1;
                   yon = true;
               }
               else if ((e.getKeyCode() == 39)){
                   xmove = 1;
                   xon = true;
                   rightoverride = true;
               }
               else if (e.getKeyCode() == 40){
                   ymove = 1;
                   yon = true;
               }
               else if ((e.getKeyCode() == 90) && (jump == true) /*&& (yon == false)*/ && (canJump == true)){
                   yon = true;
                   jumpthres = 0;
                   canJump = false;
                   //jump = true;
               }
               else if (e.getKeyCode() == 88){
                   x = 50;
                   y = 50;
               }
           }

           @Override
           public void keyReleased(KeyEvent e) {
              if ((e.getKeyCode() == 37)){ 
                  if (rightoverride == true)
                      xmove = 1;
                  else
                      xon = false;
                   leftoverride = false;
              }
              else if ((e.getKeyCode() == 39)){
                  if (leftoverride == true)
                      xmove = -1;
                  else
                      xon = false;
                  rightoverride = false;
              }
              else if ((e.getKeyCode() == 38) || (e.getKeyCode() == 40))
                   yon = false;
              else if (e.getKeyCode() == 90){
                  jumpthres = 100;
                  canJump = true;
              }
           }
       });
      
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.pack();
      frame.setResizable(false);
      frame.setVisible(true);
      
      canvas.createBufferStrategy(2);
      bufferStrategy = canvas.getBufferStrategy();
      
      canvas.requestFocus();
      
   }
   
        
   private class MouseControl extends MouseAdapter{
      
   }
   
   long desiredFPS = 60;
    long desiredDeltaLoop = (1000*1000*1000)/desiredFPS;
    
   boolean running = true;
   
   public void run(){
      
      long beginLoopTime;
      long endLoopTime;
      long currentUpdateTime = System.nanoTime();
      long lastUpdateTime;
      long deltaLoop;
      
      while(running){
         beginLoopTime = System.nanoTime();
         
         render();
         
         lastUpdateTime = currentUpdateTime;
         currentUpdateTime = System.nanoTime();
         update((int) ((currentUpdateTime - lastUpdateTime)/(5000*1000)));
         
         endLoopTime = System.nanoTime();
         deltaLoop = endLoopTime - beginLoopTime;
           
           if(deltaLoop > desiredDeltaLoop){
           }else{
               try{
                   Thread.sleep((desiredDeltaLoop - deltaLoop)/(1000*1000));
               }catch(InterruptedException e){
               }
           }
      }
   }
   
   private void render() {
      Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();
      g.clearRect(0, 0, WIDTH, HEIGHT);
      render(g);
      g.dispose();
      bufferStrategy.show();
   }
   
   

   protected void update(int deltaTime){
   /*    if ((testval == 500) && (checker.getSprite(9).getXmove() > 0))
          checker.getSprite(9).setXmove(-1);
       else if ((testval == 150) && (checker.getSprite(9).getXmove() < 0))
          checker.getSprite(9).setXmove(1);
       testval += checker.getSprite(9).getXmove();
       checker.updateObject(9, testval, 450, 50, 200, true, false, true, true, checker.getSprite(9).getXmove(), checker.getSprite(9).getYmove());
       if (checker.isOnTop(9, user)){
           x += checker.getSprite(9).getXmove();
       }*/
       
       jump = false;
       if (yon == false)
          ymove = 0;
      
      yon = true;
      ymove = 1 * ((falltime/34)+1);
      
      System.out.println(ymove + "  " + falltime);
      tempmove = xmove;
      if (xon == false)
          tempmove = 0;
       if(checker.checkBoxX(user, (int)((tempmove * deltaTime)-1), (int)((ymove*deltaTime)-1))){
            xon = false;
            x = user.updateX();
      }
      /*GOAL FOR Y COLLU:
       * 
       */
    if (jumpthres <= 99){
          ymove = (-(3 - (jumpthres/34)));
        jumpthres -= (int)(ymove * ((deltaTime * 1)-1));
          yon = true;
      }
    
      
      
      if (checker.checkBoxY(user, (int)((tempmove * deltaTime)-1), (int)((ymove*deltaTime)-1))){
          yon = false;
          y = user.updateY();
          jump = true;
          jumpthres = 100;
          falltime = 0;
      }
      if (user.isDead()){
          user.updatePlayer(1, 1, false, false, 0, 0);
          x = 1;
          y = 1;
          yon = true;
          xon = false;
          ymove = 1;
          xmove = 1;
          user.setDead(false);

      }
  //    if (jumpthres > 48)
  //        jumpthres = 0;
      
          //System.out.println(deltaTime);
      if (yon == true)
          y += (int)(ymove * ((deltaTime * 1)-1));
      if (xon == true)
          x += (int)(xmove * ((deltaTime * 1)-1));
      checker.moveObjects();
      testval++;
      System.out.println("Moving y at: " + (int)(ymove * ((deltaTime * 1)-1)));
      System.out.println("With a deltatime of " + deltaTime + " and ymove of " + ymove);
      if ((falltime < 100) && (jumpthres >= 90))
        falltime++;
/*      if (jump == true){
           if (jumpstart == 0)
               jumpstart = y + (ymove * (deltaTime * 1));
           if ((jumpstart - y) >= jumpthres){
              yon = false;
              jump = false;
              jumpstart = 0;
         }
      }*/
     /* else if (fall == true)
      if ((fall == true) && (fallstart == 0)){
          yon = true;
          ymove = 5;
          fallstart = y;
      }
      if ((fall == true) && ((y - fallstart) >= jumpthres)){
          yon = false;
          fall = false;
          fallstart = 0;
      }*/
      
  /*    if ((jump == false)){
          yon = true;
          ymove = 1;
      }*/
   /*   else if ((jump == false) && (y >= grassy - 24)){
          if (y > grassy - 24)
              y = grassy - 24;
          yon = false;
      } 
      if(grassy < 300){
         grassy++;
      }*/
      if (lefttime < 40)
          lefttime+=2;
      else
          lefttime = 0;
      if (righttime < 40)
          righttime+=2;
      else
          righttime = 0;
      if (idletime < 40)
          idletime++;
      else
          idletime = 0;
      user.updatePlayer(x, y, xon, yon, xmove, ymove);
      System.out.println("x: " + x + " y: " + y);
      prevDelta = deltaTime;
   }
   
   protected void render(Graphics2D g){

     /*  g.fillRect(0,150,390,30,true,false,false,false,0,0);
g.fillRect(30,150,30,300,true,false,false,false,0,0);
g.fillRect(300,90,60,30,true,false,false,false,0,0);
g.fillRect(300,180,30,270,true,false,false,false,0,0);
g.fillRect(330,90,20,60,true,false,false,false,0,0);
g.fillRect(480,90,20,60,true,false,false,false,0,0);
g.fillRect(540,90,90,30,true,false,false,false,0,0);
g.fillRect(630,90,120,120,true,false,false,false,0,0);
g.fillRect(630,300,120,120,true,false,false,false,0,0);
g.fillRect(540,330,30,60,true,false,false,false,0,0);
g.fillRect(540,300,30,30,true,false,false,false,0,0);
g.fillRect(207,300,90,30,true,false,false,false,0,0);
g.fillRect(361,300,20,90,true,false,false,false,0,0);
g.fillRect(237,300,20,60,true,false,false,false,0,0);
g.fillRect(30,510,30,570,true,false,false,false,0,0);
g.fillRect(720,420,90,30,true,false,false,false,0,0);
g.fillRect(168,30,12,210,true,true,false,false,0,0);
g.fillRect(570,100,210,10,true,true,false,false,0,0);
g.fillRect(620,100,210,10,true,true,false,false,0,0);
g.fillRect(570,318,12,60,true,true,false,false,0,0);
g.fillRect(30,225,240,12,true,true,false,false,0,0);
g.fillRect(294,491,6,34,true,true,false,false,0,0);
g.fillRect(497,431,6,34,true,true,false,false,0,0);
g.fillRect(237,371,6,393,true,true,false,false,0,0);
g.fillRect(696,420,90,30,true,true,false,false,0,0);
g.fillRect(0,750,30,750,true,true,false,false,0,0);*/
       
      g.setColor(Color.green); 
      
      g.drawImage(background, 0, 0, frame);
  //    g.fillRect(testval, 450, 200, 50);
      if (xon == true){
          if (xmove > 0){
              if (righttime < 20)
                  g.drawImage(playerrightstride, (int)x, (int)y, frame);
              else
                  g.drawImage(playerrightstridealt, (int)x, (int)y, frame);
          }
          else{
              if (lefttime < 20)
                  g.drawImage(playerleftstride, (int)x, (int)y, frame);
              else
                  g.drawImage(playerleftstridealt, (int)x, (int)y, frame);
          }
      }
      else{
          if (xmove > 0){
              if (idletime < 20)
                  g.drawImage(playerright, (int)x, (int)y, frame);
              else
                  g.drawImage(playerrightalt, (int)x, (int)y, frame);
          }
          else{
              if (idletime < 20)
                  g.drawImage(playerleft, (int)x, (int)y, frame);
              else
                  g.drawImage(playerleftalt, (int)x, (int)y, frame);
          }
      }
      /*  g.fillRect(0,400,150,30);
        g.fillRect(150,370,30,60);
        g.fillRect(180,370,90,30);
        g.fillRect(270,370,30,19);
        g.fillRect(340,400,90,19);*/
      //g.setColor(Color.green);
      //g.fillRect(0, (int)grassy, 750, 1);
   }
   
   
   public static void main(String [] args){
      Game_v_03 ex = new Game_v_03();
      new Thread(ex).start();
   }

}
