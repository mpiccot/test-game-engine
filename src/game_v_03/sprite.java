/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game_v_03;

/**
 *
 * @author Mike
 */
public class sprite {
    private int x, y, height, width;
    private boolean isWall, isFatal, xon, yon, jump, isDead = false;
    private int xmove, ymove, updatedX, updatedY;
    sprite(int X, int Y, int HEIGHT, int WIDTH, boolean ISWALL, boolean ISFATAL, boolean XON, boolean YON, int XMOVE, int YMOVE){
        x = X;
        y = Y;
        height = HEIGHT;
        width = WIDTH;
        isWall = ISWALL;
        isFatal = ISFATAL;
        xon = XON;
        yon = YON;
        xmove = XMOVE;
        ymove = YMOVE;
    }
    public void updateSprite(int X, int Y, int HEIGHT, int WIDTH, boolean ISWALL, boolean ISFATAL, boolean XON, boolean YON, int XMOVE, int YMOVE){
        x = X;
        y = Y;
        height = HEIGHT;
        width = WIDTH;
        isWall = ISWALL;
        isFatal = ISFATAL;
        xon = XON;
        yon = YON;
        xmove = XMOVE;
        ymove = YMOVE;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getHeight(){
        return height;
    }
    public int getWidth(){
        return width;
    }
    public boolean getXon(){
        return xon;
    }
    public boolean getYon(){
        return yon;
    }
    public int getXmove(){
        if (xon == true)
            return xmove;
        else
            return 0;
    }
    public void setXmove(int XMOVE){
        xmove = XMOVE;
    }
    public void setYmove(int YMOVE){
        ymove = YMOVE;
    }
    public int getYmove(){
        if (yon == true)
            return ymove;
        else
            return 0;
    }
    public void updatePlayer(int X, int Y, boolean XON, boolean YON, int XMOVE, int YMOVE){
        x = X;
        y = Y;
        xon = XON;
        yon = YON;
        xmove = XMOVE;
        ymove = YMOVE;
    }
    public boolean canJump(){
        return jump;
    }
    public void setJump(boolean canJump){
        jump = canJump;
    }
    public int updateX(){
        return updatedX;
    }
    public int updateY(){
        return updatedY;
    }
    public void changeX(){
        x += xmove;
    }
    public void changeY(){
        y += ymove;
    }
    public void setX(int x){
        updatedX = x;
    }
    public void setY(int y){
        updatedY = y;
    }
    public boolean getFatal(){
        return isFatal;
    }
    public void setDead(boolean ISDEAD){
        isDead = ISDEAD;
    }
    public boolean isDead(){
        return isDead;
    }
}
