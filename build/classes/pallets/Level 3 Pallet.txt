VARIABLE NAMES:
checker.addObject(x , y  , height, width, isWall, isFatal, xon  , yon  , xmove, ymove)

TEMPLATE:
checker.addObject(__,____,_______,______, true  , false  , false, false, 0    , 0    )

EXAMPLE:
checker.addObject(10, 280, 750   , 5    , true  , false  , false, false, 0    , 0    )


checker.addObject(0,150,390,30,true,false,false,false,0,0)
checker.addObject(30,150,30,300,true,false,false,false,0,0)
checker.addObject(300,90,60,30,true,false,false,false,0,0)
checker.addObject(300,180,30,270,true,false,false,false,0,0)
checker.addObject(330,90,20,60,true,false,false,false,0,0)
checker.addObject(480,90,20,60,true,false,false,false,0,0)
checker.addObject(540,90,90,30,true,false,false,false,0,0)
checker.addObject(630,90,120,120,true,false,false,false,0,0)
checker.addObject(630,300,120,120,true,false,false,false,0,0)
checker.addObject(540,330,30,60,true,false,false,false,0,0)
checker.addObject(540,300,30,30,true,false,false,false,0,0)
checker.addObject(207,390,30,450,true,false,false,false,0,0)     
checker.addObject(207,300,90,30,true,false,false,false,0,0)
checker.addObject(361,300,20,90,true,false,false,false,0,0)
checker.addObject(237,300,20,60,true,false,false,false,0,0)
checker.addObject(30,510,30,570,true,false,false,false,0,0)
checker.addObject(720,420,90,30,true,false,false,false,0,0)
checker.addObject(330,168,12,210,true,true,false,false,0,0)
checker.addObject(570,100,110,10,true,true,false,false,0,0)
checker.addObject(620,100,110,10,true,true,false,false,0,0)
checker.addObject(570,318,12,60,true,true,false,false,0,0)
checker.addObject(30,225,240,12,true,true,false,false,0,0)
checker.addObject(154,492,6,34,true,true,false,false,0,0)
checker.addObject(497,431,6,34,true,true,false,false,0,0)
checker.addObject(237,371,6,393,true,true,false,false,0,0)
checker.addObject(696,420,90,30,true,true,false,false,0,0)
checker.addObject(0,750,30,750,true,true,false,false,0,0)