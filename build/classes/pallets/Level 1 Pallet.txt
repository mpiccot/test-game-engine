VARIABLE NAMES:
checker.addObject(x , y  , height, width, isWall, isFatal, xon  , yon  , xmove, ymove)

TEMPLATE:
checker.addObject(__,____,_______,______, true  , false  , false, false, 0    , 0    )

EXAMPLE:
checker.addObject(10, 280, 750   , 5    , true  , false  , false, false, 0    , 0    )


checker.addObject(0,400,30,150,true,false,false,false,0,0)
checker.addObject(150,370,60,30,true,false,false,false,0,0)
checker.addObject(180,370,30,90,true,false,false,false,0,0)
checker.addObject(270,370,19,30,true,false,false,false,0,0)
checker.addObject(340,400,19,90,true,false,false,false,0,0)